
-define(DEFAULT_MAP_WIDTH, 100).
-define(DEFAULT_MAP_HEIGHT, 100).
-define(DEFAULT_MAX_PLAYERS, 10).

-define(DIRECTIONS, [ 'UP', 'DOWN', 'LEFT', 'RIGHT',
    'UP-LEFT', 'UP-RIGHT', 'DOWN-LEFT', 'DOWN-RIGHT']).
-define(DIRECTIONS_COUNT, 8).

%% Encapsulates player details
-record(player, {pid :: pid(),
                 pos = undefined :: {non_neg_integer(), non_neg_integer(), atom()},
                 last_action_ts = 0 :: non_neg_integer(),
                 killed_list = [] :: list(pid())
}).

%% Encapsulates game details
-record(game, {pid :: pid(),
               state :: 'init' | 'ended',
               started_time :: non_neg_integer()}).
