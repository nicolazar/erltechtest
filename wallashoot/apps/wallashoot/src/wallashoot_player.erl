%%%-------------------------------------------------------------------
%%% @author Nicoleta Lazar
%%% @doc wallashoot player module. Manages the possible actions of a
%%% player.
%%% @end
%%%-------------------------------------------------------------------
-module(wallashoot_player).

-behaviour(gen_server).

%% API
-export([start/1,
         stop/1,
         move_up/1,
         move_down/1,
         move_left/1,
         move_right/1,
         move_up_left/1,
         move_up_right/1,
         move_down_left/1,
         move_down_right/1,
         shoot/1,
         join_game/1]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(state, {name :: string()}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%% @end
%%--------------------------------------------------------------------
-spec(start(Opts :: list()) ->
  {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start(Name) ->
  gen_server:start(?MODULE, [Name], []).

%%--------------------------------------------------------------------
%% @doc
%% Stops the server
%% @end
%%--------------------------------------------------------------------
-spec(stop(Pid :: pid()) -> ok).
stop(Pid) ->
  gen_server:call(Pid, stop).

%%--------------------------------------------------------------------
%% @doc
%% Allows a player identified by PlayerName to join a game.
%% If no spots are left in the game, exits with a descriptive message.
%% The initial position of the player is generated randomly by
%% the game process.
%% @end
%%--------------------------------------------------------------------
-spec(join_game(PlayerName :: string()) -> ok | {stop, pid()}).
join_game(PlayerName) ->
  {ok, Pid} = start(PlayerName),
  GamePid = whereis(wallashoot_game),
  Reply = gen_server:call(GamePid, {new_player, Pid}),
  case Reply of
    {allow, _Pid, Pos} ->
      lager:log(info, ?MODULE, "Welcome, ~p. Your current possition is ~p", [PlayerName, Pos]);
    {deny, no_spots_left} ->
      lager:log(warning, ?MODULE, "No spots left for this game, please try again later"),
      stop(Pid)
  end.

%%--------------------------------------------------------------------
%% @doc
%% Allows the player with pid 'PlayerPid' to move up
%% @end
%%--------------------------------------------------------------------
-spec(move_up(PlayerPid :: pid()) -> ok).
move_up(PlayerPid) ->
  move(PlayerPid, 'UP').

%%--------------------------------------------------------------------
%% @doc
%% Allows the player with pid 'PlayerPid' to move down
%% @end
%%--------------------------------------------------------------------
-spec(move_down(PlayerPid :: pid()) -> ok).
move_down(PlayerPid) ->
  move(PlayerPid, 'DOWN').

%%--------------------------------------------------------------------
%% @doc
%% Allows the player with pid 'PlayerPid' to move left
%% @end
%%--------------------------------------------------------------------
-spec(move_left(PlayerPid :: pid()) -> ok).
move_left(PlayerPid) ->
  move(PlayerPid, 'LEFT').

%%--------------------------------------------------------------------
%% @doc
%% Allows the player with pid 'PlayerPid' to move right
%% @end
%%--------------------------------------------------------------------
-spec(move_right(PlayerPid :: pid()) -> ok).
move_right(PlayerPid) ->
  move(PlayerPid, 'RIGHT').

%%--------------------------------------------------------------------
%% @doc
%% Allows the player with pid 'PlayerPid' to move up-left
%% @end
%%--------------------------------------------------------------------
-spec(move_up_left(PlayerPid :: pid()) -> ok).
move_up_left(PlayerPid) ->
  move(PlayerPid, 'UP-LEFT').

%%--------------------------------------------------------------------
%% @doc
%% Allows the player with pid 'PlayerPid' to move up-right
%% @end
%%--------------------------------------------------------------------
-spec(move_up_right(PlayerPid :: pid()) -> ok).
move_up_right(PlayerPid) ->
  move(PlayerPid, 'UP-RIGHT').

%%--------------------------------------------------------------------
%% @doc
%% Allows the player with pid 'PlayerPid' to move down-left
%% @end
%%--------------------------------------------------------------------
-spec(move_down_left(PlayerPid :: pid()) -> ok).
move_down_left(PlayerPid) ->
  move(PlayerPid, 'DOWN-LEFT').

%%--------------------------------------------------------------------
%% @doc
%% Allows the player with pid 'PlayerPid' to move down-right
%% @end
%%--------------------------------------------------------------------
-spec(move_down_right(PlayerPid :: pid()) -> ok).
move_down_right(PlayerPid) ->
  move(PlayerPid, 'DOWN-RIGHT').

%%--------------------------------------------------------------------
%% @doc
%% Allows the player with pid 'PlayerPid' to shoot.
%% @end
%%--------------------------------------------------------------------
-spec(shoot(PlayerPid :: pid()) -> ok).
shoot(PlayerPid) ->
  GamePid = whereis(wallashoot_game),
  Reply = gen_server:call(GamePid, {shoot, PlayerPid}),
  case Reply of
    {ok, no_player_shot} ->
      lager:log(info, ?MODULE, "Player ~p: You killed no player this time! Try again!", [PlayerPid]);
    {ok, SPid} ->
      lager:log(info, ?MODULE, "Player ~p: You killed ~p! Congratulations!", [PlayerPid, SPid]);
    {ok, winner, PlayerPid} ->
      lager:log(info, ?MODULE, "Player ~p: Congratulations! You won the game!", [PlayerPid]);
    {error, action_denied} ->
      lager:log(warning, ?MODULE, "Player ~p: Action denied; you can only move or shoot minimum every 100 ms by default!", [PlayerPid]);
    Other ->
      lager:log(warning, ?MODULE, "Player ~p: Unexpected reply", [PlayerPid, Other])
  end.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Allows the player with pid 'PlayerPid' to move to the direction
%% 'Direction'.
%% @end
%%--------------------------------------------------------------------
-spec(move(PlayerPid :: pid(), Direction :: atom()) -> ok).
move(Player, Direction) ->
  GamePid = whereis(wallashoot_game),
  Reply = gen_server:call(GamePid, {move, Player, Direction}),
  case Reply of
    {ok, Pos} ->
      lager:log(info, ?MODULE, "Player ~p: Successfully moved to ~p", [Player, Pos]);
    {error, action_denied} ->
      lager:log(warning, ?MODULE, "Player ~p: Action denied; you can only move or shoot minimum every 100 ms by default!", [Player]);
    {error, position_unavailable} ->
      lager:log(warning, ?MODULE, "Player ~p: Action denied; the position you are trying to move to is unavailable!", [Player]);
    Other ->
      lager:log(warning, ?MODULE, "Player ~p: Unexpected reply", [Player, Other])
  end.


%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
  {ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term()} | ignore).
init([Name]) ->
  lager:log(notice, ?MODULE, "Initializing new player with name ~p", [Name]),
  {ok, #state{name = Name}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handles call messages
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #state{}) ->
  {reply, Reply :: term(), NewState :: #state{}} |
  {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_call(stop, _From, State) ->
  {stop, normal, ok, State};
handle_call(_Request, _From, State) ->
  {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handles cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_cast(_Request, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handles all non call/cast messages
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_info(_Info, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(_Reason, _State) ->
  ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
    Extra :: term()) ->
  {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

