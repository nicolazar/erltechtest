%%%-------------------------------------------------------------------
%%% @author Nicoleta Lazar
%%% @doc wallashoot game - Manages game logic
%%% @end
%%%-------------------------------------------------------------------
-module(wallashoot_game).

-behaviour(gen_server).

-include("../include/wallashoot.hrl").

%% API
-export([start/1,
         stop/1]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).
-define(DEFAULT_TIMEOUT, 100). %% milliseconds

-record(state, {map_height :: non_neg_integer(),
                map_width :: non_neg_integer(),
                max_players :: non_neg_integer(),
                current_players_num = 0 :: non_neg_integer(),
                killed_players_num = 0 :: non_neg_integer(),
                players_dict = dict:new(),
                game_started :: boolean(),
                winner :: #player{}}).


%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%% @end
%%--------------------------------------------------------------------
-spec(start(Opts :: list()) ->
  {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start(Opts) ->
  gen_server:start({local, ?SERVER}, ?MODULE, [Opts], []).

%%--------------------------------------------------------------------
%% @doc
%% Stops the server
%% @end
%%--------------------------------------------------------------------
-spec(stop(Pid :: pid()) -> ok).
stop(Pid) ->
  gen_server:call(stop, Pid).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
  {ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term()} | ignore).
init([Opts]) ->
  process_flag(trap_exit, true),
  lager:log(info, ?MODULE, "Initialising a new game with opts ~p", [Opts]),

  MapWidth = proplists:get_value(game_map_width, Opts, ?DEFAULT_MAP_WIDTH),
  MapHeight = proplists:get_value(game_map_height, Opts, ?DEFAULT_MAP_HEIGHT),
  MaxPlayers = proplists:get_value(max_players, Opts, ?DEFAULT_MAX_PLAYERS),

  {ok, #state{map_width = MapWidth,
              map_height = MapHeight,
              max_players = MaxPlayers,
              game_started = false,
              winner = undefined}}.


%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handles call messages
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #state{}) ->
  {reply, Reply :: term(), NewState :: #state{}} |
  {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_call({new_player, _Name}, _From, #state{current_players_num = CurrPlayers,
  max_players = MaxPlayers} = State) when MaxPlayers =:= CurrPlayers ->
  {reply, {deny, no_spots_left}, State};

handle_call({new_player, Pid}, _From, State) ->
  #state{players_dict = PlayersDict, current_players_num = CurrPlayers, game_started = GameStarted} = State,
  Pos = generate_position(State),
  link(Pid),
  Player = #player{pid = Pid, pos = Pos},
  NewState0 = State#state{current_players_num = CurrPlayers + 1,
    players_dict = dict:store(Pid, Player, PlayersDict)},

  %% Verify if there are at least 2 players, so that the game can start
  NewState = if (GameStarted == false) and (CurrPlayers + 1 == 2) ->
    NewState0#state{game_started = true};
    true -> NewState0
  end,
  lager:log(info, ?MODULE, "A new player ~p has joined the game.", [Pid]),
  {reply, {allow, Pid, Pos}, NewState};

handle_call({move, PlayerPid, Direction}, _From, State) ->
  #state{players_dict = PlayerDict} = State,
  Player = dict:fetch(PlayerPid, PlayerDict),
  Now = wallashoot_util:now_ms(),
  Diff = Now - Player#player.last_action_ts,
  {Reply, NewPlayer} = case Diff < ?DEFAULT_TIMEOUT of
    false ->
      NewPos = try compute_new_position(PlayerPid, Direction, State) catch throw:position_unavailable -> undefined end,
      case NewPos of
        undefined ->
          {{error, position_unavailable}, Player};
        _ ->
          {{ok, NewPos}, Player#player{pos = NewPos}}
      end;
    true ->
      {{error, action_denied}, Player}
  end,
  NewPlayer1 = NewPlayer#player{last_action_ts = Now},
  {reply, Reply, State#state{players_dict = dict:store(PlayerPid, NewPlayer1, PlayerDict)}};

handle_call({shoot, PlayerPid}, _From, State) ->
  #state{players_dict = PlayersDict, killed_players_num = KilledPlayers,
         current_players_num = TotalPlayers, game_started = GameStarted} = State,
  Player = dict:fetch(PlayerPid, PlayersDict),
  #player{killed_list = KList, last_action_ts = LastActTs} = Player,
  Now = wallashoot_util:now_ms(),
  Diff = Now - LastActTs,
  case Diff < ?DEFAULT_TIMEOUT of
    false ->
      ShotPlayer = get_shot_player(Player, PlayersDict),
      case ShotPlayer of
        undefined ->
          {reply, {ok, no_player_shot}, State#state{players_dict = dict:store(PlayerPid, Player#player{last_action_ts = Now}, PlayersDict)}};
        SPid ->
          kill_player(SPid),
          NewPlayersDict0 = dict:erase(SPid, PlayersDict),
          NewPlayersDict = dict:store(PlayerPid, Player#player{last_action_ts = Now, killed_list = [SPid | KList]}, NewPlayersDict0),
          NewState = State#state{current_players_num = TotalPlayers -1,
            killed_players_num = KilledPlayers + 1,
            players_dict =  NewPlayersDict},
          case dict:size(NewPlayersDict0) of
            1 when GameStarted == true ->
              NewState0 = NewState#state{winner = Player},
              lager:log(info, ?MODULE, "Game ~p ended and we have a winner: ~p", [self(), Player]),
              {stop, normal, {ok, winner, PlayerPid}, NewState0};
            _ ->
              {reply, {ok, SPid}, NewState}

          end
      end;
    true ->
      {reply, {error, action_denied}, State#state{players_dict = dict:store(PlayerPid, Player#player{last_action_ts = Now}, PlayersDict)}}
  end;

handle_call(_Request, _From, State) ->
  {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handles cast messages
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_cast(_Request, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handles all non call/cast messages
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_info(_Info, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(normal, State) ->
  print_statistics(State),
  ok;
terminate(_Reason, _State) ->
  ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
    Extra :: term()) ->
  {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Given a current state, randomly generate a new position to assign
%% to a new player, with the property that it does not collides with an
%% already occupied position and it's between the bounds of the game
%% map.
%% @end
%%--------------------------------------------------------------------
-spec(generate_position(State :: term()) ->
  {PosX :: non_neg_integer(), PosY :: non_neg_integer(), Dir :: non_neg_integer()}).
generate_position(State) ->
  #state{map_width = Width, map_height = Height, players_dict = PlayersDict} = State,
  random:seed(erlang:phash2([node()]), erlang:monotonic_time(), erlang:unique_integer()),
  PosX = random:uniform(Width),
  PosY = random:uniform(Height),
  Dir  = lists:nth(random:uniform(?DIRECTIONS_COUNT), ?DIRECTIONS),

  %% Verify that the generated position is not already occupied
  FilteredPlayers = dict:filter(
    fun(_K, {X, Y, _D}) when {X, Y} =:= {PosX, PosY} -> true;
       (_, _) -> false end,
    PlayersDict),

  %% If no player exists on the generated position, return it,
  %% otherwise generate again.
  case dict:is_empty(FilteredPlayers) of
    true -> {PosX, PosY, Dir};
    false -> generate_position(State)
  end.

%%--------------------------------------------------------------------
%% @private
%% @doc Given a player, a direction and the current state of the
%% game, compute the new position he will be in after he would move
%% on the specified direction. Throw an error in case the move is
%% not allowed because the new position collides with an occupied
%% position.
%% @end
%%--------------------------------------------------------------------
-spec(compute_new_position(PlayerPid :: pid(), Direction :: atom(), State :: term()) ->
  {PosX :: non_neg_integer(), PosY :: non_neg_integer(), Dir :: non_neg_integer()} | error).
compute_new_position(PlayerPid, Direction, State) ->
  #state{map_height = Height, map_width = Width, players_dict = PlayersDict} = State,
  Player = dict:fetch(PlayerPid, PlayersDict),
  {PosX, PosY, _Dir} = Player#player.pos,
  {NewPosX, NewPosY} = case Direction of
    'UP' -> {PosX - 1, PosY};
    'DOWN' -> {PosX + 1, PosY};
    'LEFT' -> {PosX, PosY - 1};
    'RIGHT' -> {PosX, PosY + 1};
    'UP-LEFT' -> {PosX - 1, PosY - 1};
    'UP-RIGHT' -> {PosX - 1, PosY + 1};
    'DOWN-LEFT' -> {PosX + 1, PosY - 1};
    'DOWN-RIGHT' -> {PosX + 1, PosY + 1};
    _ -> undefined
  end,

  if (NewPosX < 0) or (NewPosY < 0) or (NewPosX > Height) or (NewPosY > Width) ->
    throw(position_unavailable);
  true ->
    %% Verify that the new position is not occupied by another player
    PlayersPositions = dict:map(fun(_K, P) -> P#player.pos end, PlayersDict),
    FilteredPlayers = dict:filter(
      fun(_K, {X, Y, _D}) when {X, Y} =:= {NewPosX, NewPosY} -> true;
        (_, _) -> false end,
      PlayersPositions),

    case dict:is_empty(FilteredPlayers) of
      true -> {NewPosX, NewPosY, Direction};
      false -> throw(position_unavailable)
    end
  end.


%%--------------------------------------------------------------------
%% @private
%% @doc Given a player pid and the current dictionary of players,
%% check if any player is shot, by verifying if any of them is
%% on the direction of the shooting.
%% @end
%%--------------------------------------------------------------------
-spec(get_shot_player(Player :: term(), PlayersDict :: term()) -> Pid :: pid() | undefined).
get_shot_player(Player, PlayersDict) ->
  #player{pid = PlayerPid, pos = {PosX, PosY, Dir}} = Player,
  PlayersPositions = dict:map(fun(_K, P) -> P#player.pos end, PlayersDict),
  CollinearPlayers = dict:filter(
    fun(K, _V) when K == PlayerPid -> false;
      (_K, {_X, Y, _D}) when (Dir == 'UP' orelse Dir == 'DOWN') andalso (Y == PosY) -> true;
      (_K, {X, _Y, _D}) when (Dir == 'RIGHT' orelse Dir == 'LEFT') andalso (X == PosX) -> true;
      (_K, {X, Y, _D}) when (Dir == 'UP-LEFT' orelse Dir == 'DOWN-RIGHT') andalso (X - PosX == Y - PosY) -> true;
      (_K, {X, Y, _D}) when (Dir == 'UP-RIGHT' orelse Dir == 'DOWN-LEFT') andalso (X - PosX == PosY - Y) -> true;
      (_K, _V) -> false end, PlayersPositions),
  case dict:is_empty(CollinearPlayers) of
    true ->
      undefined;
    false ->
      CPList = dict:to_list(CollinearPlayers),
      ShotPlayer = retrieve_closest({PosX, PosY, Dir}, CPList),
      ShotPlayer
  end.

%%--------------------------------------------------------------------
%% @private
%% @doc Given a position and the list of positions per each player,
%% return the player which is closest to the specified position.
%% @end
%%--------------------------------------------------------------------
-spec(retrieve_closest(Pos :: tuple(), PosList :: list(tuple())) -> pid()).
retrieve_closest(Pos, PosList) ->
  {PosX, PosY, _Dir} = Pos,
  SortedList = lists:sort(fun({_P, {X, Y, _D}}, {_P1, {X1, Y1, _D1}}) ->
    distance({X, Y}, {PosX, PosY}) < distance({X1, Y1}, {PosX, PosY}) end, PosList),
  [Head|_Tail] = SortedList,
  {Pid, _} = Head,
  Pid.

%%--------------------------------------------------------------------
%% @private
%% @doc Computes the distance between two 2D points.
%% @end
%%--------------------------------------------------------------------
distance({X, Y}, {PosX, PosY}) ->
  math:sqrt(math:pow((X - PosX), 2) + math:pow((Y - PosY), 2)).

%%--------------------------------------------------------------------
%% @private
%% @doc Kills a player identified by his Pid.
%% @end
%%--------------------------------------------------------------------
kill_player(Pid) ->
  wallashoot_player:stop(Pid),
  receive
    {'EXIT', Pid, Reason} ->
      lager:log(info, ?MODULE, "Player ~p terminated with reason ~p", [Pid, Reason]),
      ok
  after 2000 ->
    lager:log(error, ?MODULE, "Timed out waiting for the player ~p to die", [Pid]),
    ok
  end.

%%--------------------------------------------------------------------
%% @private
%% @doc Given the state of a game, print the statistics.
%% @end
%%--------------------------------------------------------------------
print_statistics(State) ->
  %%@TODO: Write statistics in a more elegant way.
  lager:log(info, ?MODULE, "Game statistics: ~p", [lager:pr(State, state)]).

