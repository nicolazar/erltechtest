%%%-------------------------------------------------------------------
%% @doc wallashoot top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(wallashoot_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

-define(DEFAULT_MAX_RESTART, 5).
-define(DEFAULT_MAX_TIME, 60).

%% Children specification - a tuple composed by:
%% {ChildId, StartFuncion :: {M, F, A}, Restart, Shutdown, Type, Modules}
-define(CHILD_SPEC(Mod, Type, Args),
    {Mod, {Mod, start_link, Args}, transient, 5000, Type, [Mod]}).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->

    AppEnv = application:get_all_env(wallashoot),
    lager:log(info, ?MODULE, "Initialising the top level supervisor"),

    {ok, {
        {one_for_one, ?DEFAULT_MAX_RESTART, ?DEFAULT_MAX_TIME},
        [?CHILD_SPEC(wallashoot_game_manager, worker, [AppEnv])]
    }}.

%%====================================================================
%% Internal functions
%%====================================================================
