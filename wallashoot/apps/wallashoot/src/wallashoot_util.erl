%%%-------------------------------------------------------------------
%%% @author Nicoleta Lazar
%%% @doc Utility module
%%% @end
%%%-------------------------------------------------------------------
-module(wallashoot_util).

%% API
-export([now_ms/0]).

%% @doc Get current time
now_ms() ->
  now_ms(os:timestamp()).

now_ms({Mega, Sec, Micro}) ->
  (Mega * 1000000000) + (Sec * 1000) + (Micro div 1000).
