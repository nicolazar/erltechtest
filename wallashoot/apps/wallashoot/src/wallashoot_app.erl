%%%-------------------------------------------------------------------
%% @doc wallashoot application public API
%% @end
%%%-------------------------------------------------------------------

-module(wallashoot_app).

-behaviour(application).

%% Application callbacks
-export([start/2
        ,stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
  ensure_started(lager),
  wallashoot_sup:start_link().

%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================

ensure_started(App) ->
  io:format("Starting app ~p", [App]),
  case application:start(App) of
    ok -> ok;
    {error, {already_started, App}} -> ok
  end.