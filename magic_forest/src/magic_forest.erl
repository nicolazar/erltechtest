%%%-----------------------------------------------------------------------------
%%% @author Nicoleta Lazar
%%% @doc A magic forest is a set of nodes and edges. An edge connects
%%% two distinct nodes. Two nodes cannot be connected by more than one
%%% edge. A subset of nodes is a tree if it has the following two properties:
%%%   - For any two nodes in the subset there is exactly one series of
%%%     edges Xi connecting them.
%%%   - There is no edge connecting a node from the subset to a node outside
%%%     the subset.
%%% Given a magic forest, this module returns the number of trees in the forest.
%%% @end
%%%------------------------------------------------------------------------------
-module(magic_forest).

%% API
-export([count_trees/0,
         count_trees/1]).

%% Encapsulates a graph data structure
-record(graph, {
  nodes :: non_neg_integer(),
  neighbours :: list(list())
}).

-define(DEFAULT_INPUT_FILE, "../input/magic_forest.in").

%%%===================================================================
%%% API
%%%===================================================================

%%%-------------------------------------------------------------------
%% @doc Returns the number of trees in a magic forest. Reads the input
%% from the default input file.
%% @end
%%%-------------------------------------------------------------------
-spec count_trees() -> non_neg_integer().
count_trees() ->
  count_trees(?DEFAULT_INPUT_FILE).

%%%-------------------------------------------------------------------
%% @doc Returns the number of trees in a magic forest. Reads the input
%% from the specified input file. If the input is not valid, returns
%% undefined
%% @end
%%%-------------------------------------------------------------------
-spec count_trees(InputFile :: string()) -> non_neg_integer() | undefined.
count_trees(InputFile) ->
  Graph = try process_input(InputFile) catch throw:Error ->
    io:format("Cannot compute the number of trees due to error: ~p", [Error]),
    undefined
  end,
  Result = if Graph =/= undefined ->
    traverse_graph(Graph, 0, sets:new(), 0);
    true -> undefined
  end,
  io:format("The number of trees in the magic forest is ~p~n", [Result]),
  Result.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%%%-------------------------------------------------------------------
%% @private
%% @doc Given an input file, reads the number of nodes and the
%% edges between those nodes and returns a #graph record that
%% encapsulates the data
%% @end
%%%-------------------------------------------------------------------
-spec process_input(InputFile :: string()) -> #graph{}.
process_input(InputFile) ->
  IODevice = case file:open(InputFile, [binary]) of
    {ok, Device} -> Device;
    {error, Reason} ->
      io:format("An error occurred when opening file ~p: ~p~n", [InputFile, Reason]),
      throw(Reason)
  end,

  Nodes = case io:get_line(IODevice, "") of
    {error, Err} ->
      io:format("An error occurred when reading the file ~p: ~p~n", [InputFile, Err]),
      throw(invalid_input);
    Data ->
      [Bin, _] = binary:split(Data, <<"\n">>),
      N = binary_to_integer(Bin),
      if N =< 0 ->
        io:format("Invalid input: the number of nodes must be positive!~n"),
        throw(invalid_input);
        true -> N
      end
  end,

  Neighbours = try process_lines(IODevice, []) catch throw:Error -> throw(Error) end,
  Graph = #graph{nodes = Nodes, neighbours = Neighbours},
  io:format("Finished reading input from file ~p: ~p~n", [InputFile, Graph]),
  Graph.

%%%-------------------------------------------------------------------
%% @private
%% @doc Process lines in a file given by an IODevice and returns a
%% list of tuples that encapsulates the edges of the graph or
%% throws an error in case the input is invalid.
%% @end
%%%-------------------------------------------------------------------
-spec process_lines(IODevice :: pid(), Acc :: list()) -> list(tuple) | error.
process_lines(IODevice, Acc) ->
  case io:get_line(IODevice, "") of
    eof ->
      file:close(IODevice),
      Acc;
    Line ->
      Bin = case binary:split(Line, <<"\n">>) of
        [B, <<"">>] -> B;
        [B] -> B
      end,
      case binary:split(Bin, <<" ">>) of
        [BinN1, BinN2] ->
          N1 = binary_to_integer(BinN1),
          N2 = binary_to_integer(BinN2),

          %% Add N2 in the N1 neighbours list
          NewAcc0 = case lists:keyfind(N1, 1, Acc) of
            false ->
              Acc ++ [{N1, [N2]}];
            {N1, L1} ->
              lists:keyreplace(N1, 1, Acc, {N1, [N2|L1]} )
          end,

          %% Add N1 in the N2 neighbours list
          NewAcc = case lists:keyfind(N2, 1, NewAcc0) of
            false ->
              NewAcc0 ++ [{N2, [N1]}];
            {N2, L2} ->
              lists:keyreplace(N2, 1, NewAcc0, {N2, [N1|L2]} )
          end,

          %% Continue processing lines
          process_lines(IODevice, NewAcc);
        _ ->
          io:format("The line does not encapsulates an edge, valid input is: N1 N2"),
          throw(invalid_input)
      end
  end.

%%%-------------------------------------------------------------------
%% @private
%% @doc Given a #graph{}, traverses it depth-first starting from the
%% node Node. If a terminal node is reached and no cycles were discovered
%% on the path, increase the number of trees, marking all these nodes
%% as visited. Repeat until no node remains unvisited.
%%%-------------------------------------------------------------------
traverse_graph(Graph, Node, Visited, NumTrees) when Node < Graph#graph.nodes ->
  case sets:is_element(Node, Visited) of
    true ->
      traverse_graph(Graph, Node + 1,  Visited, NumTrees);
    false ->
      {NewVisited, HasCycles} = traverse_graph_from_node(Graph, Node, undefined, Visited),
      NewNumTrees = if HasCycles == true ->
        NumTrees;
        true -> NumTrees + 1
      end,
      traverse_graph(Graph, Node + 1, NewVisited, NewNumTrees)
  end;
traverse_graph(_Graph, _Node, _Visited, NumTrees) ->
  NumTrees.

%% @private
traverse_graph_from_node(Graph, Node, Prev, Visited) ->
  Visited1 = sets:add_element(Node, Visited),
  case lists:keyfind(Node, 1, Graph#graph.neighbours) of
    false ->
      {Visited1, false};
    {Node, Neighbours} ->
      %% there are neighbours
      Map = lists:map(fun(N) ->
        if N =:= Prev ->
          {Visited1, false};
        true ->
          case sets:is_element(N, Visited1) of
            false ->
              traverse_graph_from_node(Graph, N, Node, Visited1);
            true ->
              {Visited1, true}
          end
        end
      end, Neighbours),

      lists:foldl(fun({CurrVisitedList, CurrHasCycle}, {NewVisited, HasCycle}) ->
        {sets:union(CurrVisitedList, NewVisited), CurrHasCycle or HasCycle} end, {sets:new(), false}, Map)
  end.
